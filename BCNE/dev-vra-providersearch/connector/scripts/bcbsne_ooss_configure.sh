#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./bcbsne_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** BCBSNE SH : bcbsne_ooss_configure.sh start on ${HOST_NAME} **********"

    echo "Start configuring  bcbsne_ooss job"

        oossConfigFile=$HE_DIR/bcbsne-oosSupplier/resources/config/bcbsne-oos-supplier-config.json
        chmod -R 755 $oossConfigFile

        sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $oossConfigFile

        extractStartEndTime=$(date +"%Y-%m-%dT%T.%3NEDT")
        echo "extractStartEndTime: ${extractStartEndTime}"

        sed -i -e "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $oossConfigFile
        sed -i -e "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $oossConfigFile


        configStagingDir=$(grep "extractConfigStagingDir" $HE_DIR/etc/com.healthedge.customer.generic.configuration.cfg | sed -e "s/extractConfigStagingDir=//g")
        echo "configStagingDir: ${configStagingDir}"
        mkdir -p $configStagingDir

        extractConfigRequestDir=$(grep "extractConfigRequestDir" $HE_DIR/etc/com.healthedge.customer.generic.configuration.cfg | sed -e "s/extractConfigRequestDir=//g")
        echo "extractConfigRequestDir: ${extractConfigRequestDir}"
        mkdir -p $extractConfigRequestDir

        jobName=$(grep "jobName" $oossConfigFile | sed -e "s/jobName//g;s/[\", :]//g")
        
        # "s/-/\./g"           - replace '-' with '.'
        # "s/\(.*\)/\L\1/g"    - convert to lower case
        configJobName=$(sed -e "s/-/\./g;s/\(.*\)/\L\1/g" <<< "$jobName")
        
        cfgFile=$HE_DIR/etc/com.healthedge.customer.generic.extract.${configJobName}.cfg
        if [ ! -f "$cfgFile" ]
        then
            echo "Dropping bcbsne_ooss job configuration json ${oossConfigFile} to ${extractConfigRequestDir}"
            cp $oossConfigFile $extractConfigRequestDir/bcbsne-ooss-job-config.json
        else
            echo "${jobName} is already configured"
        fi

    echo "End configuring  bcbsne_ooss job"
    
echo "********** BCBSNE SH : bcbsne_oossconfigure.sh end on ${HOST_NAME} **********"
echo ""
